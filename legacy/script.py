# -*- coding: utf-8 -*-
# Created by Egor Kitselyuk  on 15/12/2019
# All questions to https://t.me/ekitselyuk or telegram @ekitselyuk

import requests
import json
import sys

import src.filereader as filereader
from openpyxl import load_workbook

# Default parameters
config={
	"XLSX_FILE": "input.xlsx",
	"RESULT_FILE" : "output.csv",
	"INN_COLUMN" : "B",
	"OGRN_COLUMN" : "A",
	"FROM_LINE" : 1,
	"TO_LINE" : 5
}

# System parameters
SUCCESS = 200
DADATA_KEY = "9426ce133b7789bc7dc7da9dd743466b00c04e54"
URL = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party"
HEADERS={
    'Authorization': 'Token %s' % DADATA_KEY,
    'Content-Type': 'application/json',
}

def printLine(output, number, cell, inn, ogrn, status="", address="", message=""):
	line = "{0};{1};{2};{3};{4};{5};{6};\n".format(number, cell, inn, ogrn, status, address, message)
	output.write(line)
	print(line)
	return

def request(output, index, cell, inn, ogrn):
	data = '{"query":%s,"branch_type":"MAIN"}' % inn
	response = requests.post(URL,  data=data,  headers=HEADERS)

	if not response.status_code == SUCCESS:
		printLine(output=output, number=index, cell=cell, inn=inn, ogrn=ogrn, message="Bad request")
		return 

	result = json.loads(response.content.decode('utf-8'))

	if not result:
		printLine(output=output, number=index, cell=cell, inn=inn, ogrn=ogrn, message="Bad format")
		return

	if not result["suggestions"]:
		printLine(output=output, number=index, cell=cell, inn=inn, ogrn=ogrn, message="No result")
		return

	suggestions_count = 0
	for suggestion in result["suggestions"]:
		if suggestion["data"]["type"] == "LEGAL":

			if suggestion["data"]["type"] == "LEGAL" and not suggestion["data"]["branch_type"] == "MAIN":
				#skip not main branch
				return

			if not suggestion["data"]["inn"] == inn:
				printLine(output=output, number=index, cell=cell, inn=inn, ogrn=ogrn, message="ИНН не совпадает")
				return

			if not suggestion["data"]["ogrn"] == ogrn:
				printLine(output=output, number=index, cell=cell, inn=inn, ogrn=ogrn, message="ОГРН не совпадает")
				return


			printLine(output=output, number=index, cell=cell, status=suggestion["data"]["state"]["status"], address=suggestion["data"]["address"]["unrestricted_value"], inn=inn, ogrn=ogrn)
		elif suggestion["data"]["type"] == "INDIVIDUAL":

			if not suggestion["data"]["inn"] == inn:
				printLine(output=output, number=index, cell=cell, inn=inn, ogrn=ogrn, message="ИНН не совпадает")
				return

			if suggestion["data"]["ogrn"] == ogrn:
				printLine(output=output, number=index, cell=cell, status=suggestion["data"]["state"]["status"], address=suggestion["data"]["address"]["unrestricted_value"], inn=inn, ogrn=ogrn)
				return
			else:
				suggestions_count = suggestions_count + 1
		else:
			printLine(output=output, number=index, cell=cell, inn=inn, ogrn=ogrn, message="Unknown type - {0}".format(suggestion["data"]["type"]))
			return


	if suggestions_count > 0:
		printLine(output=output, number=index, cell=cell, inn=inn, ogrn=ogrn, message="Not found ogrn for inn")

	return


path = ''
if len(sys.argv) > 1:
	path = sys.argv[1]

# read config
config = filereader.config(path, 'config.txt')
print("Config File:" + config)

# open file 
wb = load_workbook(path + config["EXCEL_FILE"])
ws = wb.active
output = open(path + config["RESULT_FILE"], "w+")

printLine(output=output, number="Номер", cell="Номер строки", inn="ИНН", ogrn="ОГРН", status="Статус", address="Адрес", message="Комментарий")
 
index = 0
for x in range(int(config["FROM_LINE"]), int(config["TO_LINE"]) + 1):
	index = index + 1
	inn = str(ws["{0}{1}".format(config["INN_COLUMN"], x)].value)#.encode("utf-8")
	ogrn = str(ws["{0}{1}".format(config["OGRN_COLUMN"], x)].value)#.encode("utf-8")
	request(output, index, x, inn, ogrn)



	