class Line:
	FORMAT = "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}"

	def __init__(self, number, cell, unit, message = ""):
		self.number = number
		self.cell = cell
		self.unit = unit
		self.message = message

	def toString(self):
		category_name = ""
		plan_days = ""
		plan_hours = ""

		categoty_int = int(self.unit.category)

		if categoty_int == Unit.CATEGORY_TITLE:
			category_name = "Категория"
			plan_days = "Срок проведения плановой проверки рабочих дней"
			plan_hours = "Срок проведения плановой проверки рабочих часов (для МСП и МКП)"
		elif categoty_int == Unit.CATEGORY_NONE:
			category_name = "Не является субъектом МСП"
			plan_days = "10"
			plan_hours = "0"
		elif categoty_int == Unit.CATEGORY_MICRO:
			category_name = "Микропредприятие"
			plan_days = "0"
			plan_hours = "15"
		elif categoty_int == Unit.CATEGORY_SMALL:
			category_name = "Малое предприятие"
			plan_days = "0"
			plan_hours = "50"
		elif categoty_int == Unit.CATEGORY_MEDIUM:
			category_name = "Среднее предприятие"
			plan_days = "10"
			plan_hours = "0"
		elif categoty_int == Unit.CATEGORY_EMPTY:
			category_name = "Нет информации в МСП"
			plan_days = "10"
			plan_hours = "0"
		else:
			category_name = self.unit.category
			plan_days = "10"
			plan_hours = "0"
		 

		return Line.FORMAT.format(self.number, 
			self.cell, 
			self.unit.inn, 
			self.unit.ogrn, 
			self.unit.status, 
			"\"" + self.unit.address + "\"", 
			self.unit.name, 
			category_name, 
			plan_days,
			plan_hours,
			self.message)

	def title():
		return Line("Номер", "Номер строки", Unit("ИНН", "ОГРН", "Статус", "", address = "Адрес", name = "Название", category = Unit.CATEGORY_TITLE), message = "Комментарий").toString()

class Unit:
	CATEGORY_EMPTY = -2
	CATEGORY_TITLE = -1
	CATEGORY_NONE = 0
	CATEGORY_MICRO = 1
	CATEGORY_SMALL = 2
	CATEGORY_MEDIUM = 3

	def __init__(self, inn, ogrn, status = "", org_type = "" , address = "", name = "", category = -2):
		self.inn = inn
		self.ogrn = ogrn
		self.status = status
		self.type = org_type
		self.address = address
		self.name = name
		self.category = category

	def set_category(self, category):
		self.category = category
	

	def multiUnit(units):
		inn = ""
		ogrn = ""
		status = ""
		org_type = ""
		address = ""
		name = ""
		category = ""

		if (len(units) > 0):
			for unit in units:
				inn += "%s\n" % unit.inn
				ogrn += "%s\n" % unit.ogrn
				status += "%s\n" % unit.status
				org_type += "%s\n" % unit.type
				address += "%s\n" % unit.address
				name += "%s\n" % unit.name
				category += "%s\n" % unit.category

		return Unit(inn.rstrip("\n"), ogrn.rstrip("\n"), status.rstrip("\n"), org_type.rstrip("\n"), address.rstrip("\n"), name.rstrip("\n"), category.rstrip("\n"))
