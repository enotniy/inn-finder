# Created by Egor Kitselyuk  on 27/06/2020
# All questions to https://t.me/ekitselyuk or telegram @ekitselyuk

import sys
import glob
from openpyxl import load_workbook
import src.inn
        
def config(path, filename):
    config = {}
    try:
        with open(path + filename, encoding="utf-8") as fp:
            for line in fp:
                text = line.strip()
                if len(text) > 0 and not text[0] == "#":
                    words = text.split("=")
                    config[words[0].strip()] = words[1].strip()
    except IOError:
        print(path + filename + ": File not accessible")

    return config

def read_input_excel(filename, start, stop, inn_column, ogrn_column):
    wb = load_workbook(filename)
    ws = wb.active

    result = []
    index = 0
    for x in range(start, stop + 1):
        index = index + 1
        inn_value = str(ws["{0}{1}".format(inn_column, x)].value).strip()#.encode("utf-8")
        ogrn_value = str(ws["{0}{1}".format(ogrn_column, x)].value).strip()#.encode("utf-8")

        unit = src.inn.Unit(inn_value, ogrn_value)
        result.append(src.inn.Line(index, x, unit))
    return result


def find_file(path):
    return glob.glob("%s/*.xlsx" % path)