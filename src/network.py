import requests
import json
import time
import src.inn

# System parameters
SUCCESS = 200
DADATA_KEYS = [ "9426ce133b7789bc7dc7da9dd743466b00c04e54",
				"d7d6dd9a2922e082e3547d940d97002d08211070" ]

URL_DADATA = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party"
URL_NALOG = "https://rmsp.nalog.ru/search-proc.json?"

INDEX = 0

def get_headers():
	global INDEX
	key = DADATA_KEYS[INDEX % len(DADATA_KEYS)]
	INDEX += 1
	return { 'Authorization': 'Token %s' % key, 'Content-Type': 'application/json', }

def bad_response(message):
	return { 'ok': 'false', 'message': message, 'units': [] }

def good_response(units, message = ""):
	return { 'ok': 'true', 'message': message, 'units': units }

def get_unit(data):
	return src.inn.Unit(data['inn'], data['ogrn'], 
			status = data['state']['status'], 
			org_type = data["type"] , 
			address = data["address"]["unrestricted_value"],
			name = data["name"]["full_with_opf"])

def request(unit, predicate):
	result = None
	attempt = 1

	while attempt <= 5 and result == None:
		try:
			result = predicate(unit)
		except requests.exceptions.ConnectionError as e:
			result = None
			print("	! ConnectionError: Waiting few seconds and retry.".format(e))
			time.sleep(attempt * 5)
		finally:
			attempt += 1

	if (attempt > 5):
		result = bad_response("Не удалось получить данные с сервера. Нет интернета или сработал anti-fraud")

	return result;
	

def apply_dadata(unit):
	data = '{"query":"%s","branch_type":"MAIN"}' % unit.inn
	headers = get_headers()
	response = requests.post(URL_DADATA, data=data, headers=headers)

	if not response.status_code == SUCCESS:
		return bad_response("Не удалось сделать запрос")

	result = json.loads(response.content.decode('utf-8'))

	if not result:
		return bad_response("Неправильный формат ответа: 'result' not found")

	if not result["suggestions"]:
		return bad_response("ИНН не найден в базе: проверьте правильность ИНН")

	units = []
	for suggestion in result["suggestions"]:
		# only for MAIN branch, skip not main branch

		if suggestion["data"]["type"] == "LEGAL" and suggestion["data"]["branch_type"] == "MAIN":
			if not suggestion["data"]["inn"] == unit.inn:
				return bad_response("ИНН не совпадает: проверьте, что ИНН и ОГРН не перепутаны местами")

			if suggestion["data"]["ogrn"] == unit.ogrn:
				units.append(get_unit(suggestion["data"]))

		elif suggestion["data"]["type"] == "INDIVIDUAL":
			if not suggestion["data"]["inn"] == unit.inn:
				return bad_response("ИНН не совпадает: проверьте, что ИНН и ОГРН не перепутаны местами")

			if suggestion["data"]["ogrn"] == unit.ogrn:
				units.append(get_unit(suggestion["data"]))

	if len(units) == 0:
		return bad_response("Для ИНН не найден ОГРН: проверьте правильность ИНН и ОГРН")

	if len(units) > 1:
		return good_response(units, "Для данной пары ИНН и ОГРН найдено несколько совпадений")	

	return good_response(units)

def apply_nalog(units):
	inns = []
	for unit in units:
		inns.append(unit.inn)

	params = { "mode" : "inn-list", "innList": " ".join(inns), "pageSize": 100, "page": 1}
	response = requests.post(URL_NALOG, params=params, headers={ 'Content-Type': 'application/json', 'Accept': 'application/json', })

	result = json.loads(response.content.decode('utf-8'))

	if not response.status_code == SUCCESS:
		return bad_response("Не удалось сделать запрос: {0}. {1}".format(response.status_code, result))

	if not result:
		return bad_response("Неправильный формат ответа: 'result' not found")

	if not result["data"]:
		return bad_response("Кажется результат пустой")

	units = {}
	for unit in result["data"]:
		units[unit['inn']] = src.inn.Unit(unit['inn'], unit['ogrn'], category = unit['category'])
	
	return good_response(units)

