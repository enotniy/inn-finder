# Created by Egor Kitselyuk  on 15/12/2019
# All questions to https://t.me/ekitselyuk or telegram @ekitselyuk

import sys
import pprint
import src.filereader as filereader
import src.inn as inn
import src.network as network

path = ''
if len(sys.argv) > 1:
	path = sys.argv[1] + "/" 

print("\n-------")
print("ШАГ 1 : Ищу входные файлы")
print("-------\n")

#config = filereader.config(path, 'config.txt')
#print(config)
config = {}
input_path = "%sinput" % path
files = filereader.find_file(input_path)
if len(files) > 1:
	print("В папке input слишком много xlsx файлов. Оставьте один.")
	sys.exit()
elif len(files) == 0:
	print("В папке %s нет xlsx файла. Добавтьте один." % input_path)
	sys.exit()
else:
	config["EXCEL_FILE"] = files[0]
	print("Вижу файл %s. Буду работать с ним." % config["EXCEL_FILE"])

print("\n-------")
print("ШАГ 2 : Уточняем входные данные")
print("-------\n")

print("Введите номер первой строки")
config["FROM_LINE"] = input()
print("Введите номер последней строки")
config["TO_LINE"] = input()
print("Введите букву столбца с ИНН")
config["INN_COLUMN"] = input()
print("Введите букву столбца с ОГРН")
config["OGRN_COLUMN"] = input()

config["RESULT_FILE"] = "%soutput/output.csv" % path

print("\n-------")
print("ШАГ 3 : Читаю из входного файла")
print("-------\n")

try:
	lines = filereader.read_input_excel(config["EXCEL_FILE"], int(config["FROM_LINE"]), int(config["TO_LINE"]), config["INN_COLUMN"], config["OGRN_COLUMN"])
	print(str("Прочитал {0} номеров ИНН".format(len(lines))))
except Exception as e:
	print("Что-то пошло не так: {0}".format(e))
	sys.exit()


print("\n-------")
print("ШАГ 4 : Ищу инн базе по базе dadata.ru")
print("-------\n")

results = []
for line in lines:
	try:
		response = network.request(line.unit, network.apply_dadata)
		if (response["ok"] == "true"):
			print("Result:OK line:{0} INN:{1} OGRN:{2}".format(line.cell, line.unit.inn, line.unit.ogrn))
			filledLine = inn.Line(line.number, line.cell, inn.Unit.multiUnit(response['units']), response['message'])
		else:
			print("Result:ERROR line:{0} INN:{1} OGRN:{2} message:{3}".format(line.cell, line.unit.inn, line.unit.ogrn, response['message']))
			filledLine = inn.Line(line.number, line.cell, line.unit, response['message'])

	except Exception as e:
		print("Result:EXCEPTION line:{0} INN:{1} OGRN:{2} message:{3}".format(line.cell, line.unit.inn, line.unit.ogrn, e))
		filledLine = inn.Line(line.number, line.cell, line.unit, e)
	finally:
		results.append(filledLine)
	
print("Получил {0} организаций от dadata".format(len(results)))

print("\n-------")
print("ШАГ 5 : Проверяю ИНН по базе rmsp.nalog.ru")
print("-------\n")

n = 100
chuncks = []
chunck = []
count = 0
for result in results:
	chunck.append(result.unit)
	count += 1

	if count == n:
		chuncks.append(chunck)
		chunck = []
		count = 0

if len(chunck) != 0:
	chuncks.append(chunck)

print("Начинаю проверять ИНН в ЕРСМиСП по %d штук" % n)
small_units = {}
for chunck in chuncks:
	try:
		response = network.request(chunck, network.apply_nalog)
		if response["ok"] == "true":
			small_units.update(response["units"])
			print("Нашел {0} из {1}".format(len(response["units"]), n))
		else:
			print("Ошибка {0}".format(response["message"]))
	except Exception as e:
		print("Result:EXCEPTION {0}".format(e))
print("Всего нашел {0} ИНН в ЕРСМиСП".format(len(small_units)))


print("Обновляю категории")
for result in results:
	if result.unit.inn in small_units:
		small_unit = small_units[result.unit.inn]
		result.unit.set_category(small_unit.category)


print("\n-------")
print("ШАГ 6 : Вывожу результаты на экран")
print("-------\n")

print(inn.Line.title())
for result in results:
	print(result.toString())

print("\n-------")
print("ШАГ 7 : Пишу результаты в файл")
print("-------\n")

print("Пишу результаты в файл %s" % config["RESULT_FILE"])

try:
	output = open(config["RESULT_FILE"], "w+")
	output.write(inn.Line.title() + "\n")
	for result in results:
		output.write(result.toString())
		output.write("\n")
	output.close()
except Exception as e:
	print("Что-то пошло не так: {0}".format(e))
	sys.exit()


print("\n-------")
print("КОНЕЦ : Всё прошло успешно. Нажмите любую кнопку, чтобы закрыть это окно.")
print("-------\n")

