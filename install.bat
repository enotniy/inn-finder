
set current_dir = %~dp0
echo %~dp0python\python.exe

%~dp0python\python.exe -m pip install --upgrade pip
%~dp0python\python.exe -m pip install -r %~dp0requirements.txt

pause